/**
 * Instancia de axios
 */

const axios = require('axios').default
require('dotenv').config()

const URL = process.env.VUE_APP_API_URL

// Creando una nueva instancia de axios
const instance = axios.create({
  baseURL: URL,
  timeout: 30000,
})

module.exports = instance
