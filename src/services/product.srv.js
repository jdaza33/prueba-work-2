/**
 * Servicio de productos
 */

//Modules
const axios = require('../config/axios')

const listProducts = async () => {
  try {
    let { data } = await axios.get('/api/product')
    return data
  } catch (error) {
    return error
  }
}

const listProductCategories = async () => {
  try {
    let { data } = await axios.get('/api/product-category')
    return data
  } catch (error) {
    return error
  }
}

module.exports = {
  listProducts,
  listProductCategories,
}
